package etiennefullum.elementaltowerdefense.modele;

import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by etien on 25/10/16.
 */

public class Game { // SERT AUSSI DE CONTROLLEUR À LA VUE
    private boolean GameStarted;
    private int[][] grid;
    private int width;
    private int height;
    //http://beginnersbook.com/2013/12/difference-between-arraylist-and-vector-in-java/
    //car les vector ne sont pas "fail fast"
    private Vector<BasicEnemy> listEnemy;
    private ArrayList<BasicTower> listTower;
    private double cash;
    private int score;
    private int vie;
    private int level;
    private int teamSelected;
   // private final int interval = 1000; // 1 Second


    public static final int[][] waveData = new int[][]{// basicenemy/sprinter/boss [3][0]
            {5,0,0},
            {10,0,0},
            {15,0,0},
            {0,0,1},
            {10,5,0},
            {20,3,0},
            {30,0,0},
            {30,0,2},
            {20,5,0},
            {10,0,5},
            {0,15,0},
            {10,15,0},
            {0,0,5},
            {5,5,5},
            {30,0,0},
            {40,10,3},
            {0,40,0},
            {5,10,10},
            {5,15,10},
            {20,20,10}
    };

    /**
     * 2 = droite
     * 3 = bas
     * 4 = gauche
     * 5 = haut
     * 8 = occupe
     * 9 = départ
     * 10 = fin
     * */

    /*****************Variable statique du programme ******************/
    public static final int[][] easyMap = new int[][]{//[0][1] == 9
            {0, 9, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
            {0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0},
            {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
            {0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 1, 0},
            {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
            {0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0},
            {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
            {0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 1, 0},
            {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
            {0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 10, 0},

    };

    public static boolean waveStarted = false;
    public static boolean enemyMoving = false;

    public static final int[][] mediumMap = new int[][]{//... buggy

            {0, 0, 0, 0, 0, 0, 9, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 3, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0},
            {0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
            {0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 1, 0},
            {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
            {0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 3, 1, 1, 1, 1, 1, 4, 1, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 10, 10, 0, 0, 0, 0, 0, 0, 0},
    };
    public static final int[][] hardMap = new int[][]{//... buggy

            {0, 0, 0, 0, 0, 0, 9, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 10, 10, 0, 0, 0, 0, 0, 0, 0},
    };
    //


    public Game(boolean gameStarted, int[][] grid, int width, int height, Vector<BasicEnemy> listEnemy, ArrayList<BasicTower> listTower, int level) {
        GameStarted = gameStarted;
        this.grid = grid;
        this.width = width;
        this.height = height;
        this.listEnemy = listEnemy;
        this.listTower = listTower;
        this.cash = 1500; // cash de départ
        this.score = 0;
        this.vie = 30;
        this.level = 1;
        this.level = level;
    }


    public boolean isGameStarted() {
        return GameStarted;
    }

    public void setGameStarted(boolean gameStarted) {
        GameStarted = gameStarted;
    }

    public int[][] getGrid() {
        return grid;
    }

    public void setGrid(int[][] grid) {
        this.grid = grid;
    }

    public void setGridXY(int x, int y, int value ) {
        this.grid[y][x] = value;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Vector<BasicEnemy> getListEnemy() {
        return listEnemy;
    }

    public void setListEnemy(Vector<BasicEnemy> listEnemy) {
        this.listEnemy = listEnemy;
    }

    public ArrayList<BasicTower> getListTower() {
        return listTower;
    }

    public void setListTower(ArrayList<BasicTower> listTower) {
        this.listTower = listTower;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getVie() {
        return vie;
    }

    public void setVie(int vie) {
        this.vie = vie;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getTeamSelected() {
        return teamSelected;
    }

    public void setTeamSelected(int teamSelected) {
        this.teamSelected = teamSelected;
    }
    /*Getters and setters fin*/
    /*Méthode d'une partie*/

    private boolean initialiser(){
        if (this.GameStarted){
            return true;
        }
        else{
            return false;
        }
    }

    private void newWave(int niveau){
        //...
    }


    public void ajouterEnleverCash(double v){
        this.cash = this.cash - v ;
    }
    public void addScore(int v){
        this.score+=v;
    }

}
