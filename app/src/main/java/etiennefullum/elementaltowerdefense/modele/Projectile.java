package etiennefullum.elementaltowerdefense.modele;

import android.os.Handler;

import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import etiennefullum.elementaltowerdefense.Activity.GameActivity;

/**
 * Created by etien on 09/12/16.
 */
//Dois extend de basic tower pour aller chercher l'attaque des tours
public class Projectile extends BasicTower {
    private BasicTower t;
    private BasicEnemy e;
    private int speed;


    private String parentType;
    /*2 thread car calcul très fréquent*/
    public static ScheduledExecutorService threadProjectile;

    public static int gameW;
    public static int gameH;
    public static LinkedList<Projectile> projectileList = new LinkedList<Projectile>();
    public int getWhich() {
        return which;
    }
    private int atkValue;
    private int which;
    static Runnable r;

    private int slowValue;
    private double multiplier;
    //pour les tours normal et de feu
    public Projectile(int posX, int poxY, BasicTower t, int which, String parentType, int atkValue) {
        super(posX, poxY);
        /*car le projectile vien de la tour ... */
        this.setPosX(t.getPosX() * gameW);
        this.setPosY(t.getPosY() * gameH);

        this.speed = 10;
        projectileList.add(this);
        this.which = which;
        this.parentType = parentType;
        this.atkValue = atkValue;
    }
    //pour les tours d'eau
    public Projectile(int posX, int poxY, BasicTower t, BasicEnemy e, int which, String parentType, int atkValue, int slowValue) {
        super(posX, poxY);
        /*car le projectile vien de la tour ... */
        this.setPosX(t.getPosX() * gameW);
        this.setPosY(t.getPosY() * gameH);

        this.speed = 10;
        projectileList.add(this);
        this.which = which;
        this.parentType = parentType;
        this.atkValue = atkValue;
        this.slowValue = slowValue;
    }
    //pour les tours de terre
    public Projectile(int posX, int poxY, BasicTower t, BasicEnemy e, int which, String parentType, int atkValue, double multiplier) {
        super(posX, poxY);
        /*car le projectile vien de la tour ... */
        this.setPosX(t.getPosX() * gameW);
        this.setPosY(t.getPosY() * gameH);
        this.t = t;
        this.e = e;
        this.speed = 10;
        projectileList.add(this);
        this.which = which;
        this.parentType = parentType;
        this.atkValue = atkValue;
        this.multiplier = multiplier;
    }



    public String getParentType() {
        return parentType;
    }

    public void setParentType(String parentType) {
        this.parentType = parentType;
    }

    public void setWhich(int which) {
        this.which = which;
    }

    public BasicTower getT() {
        return t;
    }

    public void setT(BasicTower t) {
        this.t = t;
    }

    public BasicEnemy getE() {
        return e;
    }

    public void setE(BasicEnemy e) {
        this.e = e;
    }

    public Runnable getR() {
        return r;
    }

    public void setR(Runnable r) {
        this.r = r;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getAtkValue() {
        return atkValue;
    }

    public void setAtkValue(int atkValue) {
        this.atkValue = atkValue;
    }


     /*https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
        *Algorithe pour faire un droite entre 2 point , soie les projectile et les enemies

     */

    public static void castProjectile() throws ConcurrentModificationException{
        final Handler handler = new Handler();
        /*
        * les runnable en android de ce type ne lance pas d'erreur il faut mettre un gros try catch devant chaque.
        * http://code.nomad-labs.com/2011/12/09/mother-fk-the-scheduledexecutorservice/
        * */
        threadProjectile.scheduleAtFixedRate(new Runnable() {

            @Override
            public void run() {
                try {
                    if (GameActivity.theGame.getListEnemy().isEmpty()) {
                        projectileList = new LinkedList<Projectile>();
                    }
                    if (!projectileList.isEmpty() && !GameActivity.theGame.getListEnemy().isEmpty()) {
                        /*linkedlist+iterateur plus efficace car suppression très courante*/
                        /*Itérateur nécessaire que mes liste ne sont pas safethread  sinon on n'a une exception de modification en même temps sur 2 thread*/
                        Iterator<Projectile> proj = projectileList.iterator();
                        while (proj.hasNext()) {
                            Projectile p = proj.next();
                            if (p.getWhich() > GameActivity.theGame.getListEnemy().size()) {
                                proj.remove();
                                projectileList.removeAll(Collections.singleton(null));

                                break;

                            } else {
                                try {

                                    int x = p.getPosX();
                                    int x2 = GameActivity.theGame.getListEnemy().get(p.getWhich()).getGridX() * gameW + gameW;
                                    int y = p.getPosY();
                                    int y2 = GameActivity.theGame.getListEnemy().get(p.getWhich()).getGridY() * gameH;
                                    int w = x2 - x; //deltaX
                                    int h = y2 - y; // deltaY
                                    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;

                                    if (w < 0) {
                                        dx1 = -1 * p.getSpeed();
                                    } else if (w > 0) {
                                        dx1 = p.getSpeed();
                                    }
                                    if (h < 0) {
                                        dy1 = -1 * p.getSpeed();
                                    } else if (h > 0) {
                                        dy1 = p.getSpeed();
                                    }
                                    if (w < 0) {
                                        dx2 = -1 * p.getSpeed();
                                    } else if (w > 0) {
                                        dx2 = p.getSpeed();
                                    }

                                    int longest = Math.abs(w);
                                    int shortest = Math.abs(h);

                                    if (!(longest > shortest)) {
                                        longest = Math.abs(h);
                                        shortest = Math.abs(w);
                                        if (h < 0) dy2 = -1;
                                        else if (h > 0) dy2 = 1;
                                        dx2 = 0;
                                    }
                                    int numerator = longest;

                                    numerator += shortest;
                                    if (!(numerator < longest)) {
                                        numerator -= longest;
                                        p.setPosX(p.getPosX() + dx1);
                                        p.setPosY(p.getPosY() + dy1);
                                    } else {
                                        p.setPosX(p.getPosX() + dx2);

                                        p.setPosY(p.getPosY() + dy2);
                                    }

                            /*collision 10 px d'incertitude ... n-10<m<n+10*/


                                    if (GameActivity.theGame.getListEnemy().get(p.getWhich()).getGridX() * gameW + gameW - 10 < p.getPosX() &&
                                            p.getPosX() < GameActivity.theGame.getListEnemy().get(p.getWhich()).getGridX() * gameW + gameW + 10) {
                                        if (GameActivity.theGame.getListEnemy().get(p.getWhich()).getGridY() * gameH - 10 < p.getPosY() &&
                                                p.getPosY() < GameActivity.theGame.getListEnemy().get(p.getWhich()).getGridY() * gameH + 10) {

                                            if(p.getParentType().equals("fire")){
                                                GameActivity.theGame.getListEnemy().get(p.getWhich()).setHp(GameActivity.theGame.getListEnemy().get(p.getWhich()).getHp() - p.getAtkValue());

                                            }else if(p.getParentType().equals("water")){
                                                GameActivity.theGame.getListEnemy().get(p.getWhich()).setHp(GameActivity.theGame.getListEnemy().get(p.getWhich()).getHp() - p.getAtkValue());
                                                GameActivity.theGame.getListEnemy().get(p.getWhich()).setMouvementCooldownIndex(GameActivity.theGame.getListEnemy().get(p.getWhich()).getMouvementCooldownIndex() - p.slowValue);

                                            }else if(p.getParentType().equals("earth")){
                                                //ajoute un multiplicateur contre les boss
                                                if( GameActivity.theGame.getListEnemy().get(p.getWhich()) instanceof Boss){
                                                    GameActivity.theGame.getListEnemy().get(p.getWhich()).setHp(GameActivity.theGame.getListEnemy().get(p.getWhich()).getHp() - (int)(p.getAtkValue()*p.multiplier));
                                                }else{
                                                    GameActivity.theGame.getListEnemy().get(p.getWhich()).setHp(GameActivity.theGame.getListEnemy().get(p.getWhich()).getHp() - (p.getAtkValue()));
                                                }

                                            }else{
                                                GameActivity.theGame.getListEnemy().get(p.getWhich()).setHp(GameActivity.theGame.getListEnemy().get(p.getWhich()).getHp() - p.getAtkValue());

                                            }
                                            proj.remove();
                                            projectileList.removeAll(Collections.singleton(null));
                                        }
                                    }
                                } catch (IndexOutOfBoundsException e) {
                                    //Arrive juste si quelque chose cause probleme à cause d'un thread
                                    //C'est correcte car cela veux dire que le tire est lancer mais que l'enemie est mort avant qu'il arrive à destination...
                                    //NE devrais pas (ou très rarement) arriver à cause de l'utilisation de linkedlist et d'itérateur
                                    proj.remove();
                                    projectileList.removeAll(Collections.singleton(null));



                                }

                            }
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 0, 5, TimeUnit.MILLISECONDS);

    }

}
