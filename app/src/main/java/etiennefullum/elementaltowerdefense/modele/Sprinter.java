package etiennefullum.elementaltowerdefense.modele;

import etiennefullum.elementaltowerdefense.modele.BasicEnemy;

/**
 * Created by etien on 25/10/16.
 */

public class Sprinter extends BasicEnemy {
    public Sprinter(int hp , Boolean isAlive, int scoreValue, int posX, int posY, int Direction, int gridX, int gridY) {
        super(hp, isAlive, scoreValue, posX, posY, Direction, gridX, gridY);
        this.setSpeed(750);
        this.setCashValue(10);
    }
}
