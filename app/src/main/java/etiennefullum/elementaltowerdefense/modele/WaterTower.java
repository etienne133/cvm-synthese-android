package etiennefullum.elementaltowerdefense.modele;

import etiennefullum.elementaltowerdefense.Activity.GameActivity;

/**
 * Created by etien on 09/12/16.
 */

public class WaterTower extends BasicTower {

    private int slowValue;
    //valeur qui ralentit le movement d'un enemie
    public WaterTower(int posX, int poxY){
        super(posX,poxY);
        this.setPosX(posX);
        this.setPosY(poxY);
        this.setAtkValue(7);
        this.slowValue=550;
        System.out.println("nuvelle tour d'eau");
    }

    public void upgrade(){

        GameActivity.theGame.setCash(GameActivity.theGame.getCash()-this.getUpgradeCost());
        this.setValue(this.getValue()+this.getUpgradeCost());
        this.setAtkValue(this.getAtkValue()/100*5);
        this.setAtkspeed(this.getAtkspeed()+1);
        this.slowValue+= 150;
        addSize();

    }
    public int getSlowValue() {
        return slowValue;
    }

    public void setSlowValue(int slowValue) {
        this.slowValue = slowValue;
    }

}
