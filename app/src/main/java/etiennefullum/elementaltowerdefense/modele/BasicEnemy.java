package etiennefullum.elementaltowerdefense.modele;

import java.util.Random;

/**
 * Created by etien on 25/10/16.
 */

public class BasicEnemy extends Entity {

    private int hp;
    private int speed;
    private Boolean isAlive;
    private int scoreValue;
    private int direction;
    private int gridX;
    private int gridY;

    private int cashValue;
    private int movementCooldown;
    private int mouvementCooldownIndex;
    private float xRandomFloat;
    private float yRandomFloat;
    private  Random rfloat = new Random();


    private boolean moved;



    public BasicEnemy(int hp, Boolean isAlive, int scoreValue, int posX, int posY, int direction, int gridX, int gridY) {
        super(posX, posY);
        this.hp = hp;
        this.speed = 250;
        this.isAlive = isAlive;
        this.scoreValue = scoreValue;
        this.direction = direction;
        this.gridX = gridX;
        this.mouvementCooldownIndex = 0 ;
        this.movementCooldown=750;
        this.xRandomFloat=1;
        this.yRandomFloat=1;
        this.cashValue=5;
    }
    public void addGrid(){
        /**
         * 2 = droite
         * 3 = bas
         * 4 = gauche
         * 5 = haut
         * 9 = départ
         * */
        if(this.direction == 2){
            this.gridX+=1;

            yRandomFloat = rfloat.nextFloat()-(float)0.50;
            xRandomFloat =  rfloat.nextFloat()*2;
        }
        else if (this.direction == 3){
            this.gridY+=1;

            yRandomFloat = rfloat.nextFloat()-(float)0.50;
            xRandomFloat =  rfloat.nextFloat()*2;
        }else if (this.direction == 4){
            this.gridX-=1;

            yRandomFloat = rfloat.nextFloat()-(float)0.50;
            xRandomFloat =  rfloat.nextFloat()*2;

        }else if (this.direction == 5){
            this.gridY-=1;

            yRandomFloat = rfloat.nextFloat()-(float)0.50;
            xRandomFloat =  rfloat.nextFloat()*2;

        }

    }

    public int getMouvementCooldownIndex() {
        return mouvementCooldownIndex;
    }

    public void setMouvementCooldownIndex(int mouvementCooldownIndex) {
        this.mouvementCooldownIndex = mouvementCooldownIndex;
    }


    public int getMovementCooldown() {
        return movementCooldown;
    }

    public void setMovementCooldown(int movementCooldown) {
        this.movementCooldown = movementCooldown;
    }


    public boolean isMoved() {
        return moved;
    }

    public void setMoved(boolean moved) {
        this.moved = moved;
    }
    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public Boolean getAlive() {
        return isAlive;
    }

    public void setAlive(Boolean alive) {
        isAlive = alive;
    }

    public int getScoreValue() {
        return scoreValue;
    }

    public void setScoreValue(int scoreValue) {
        this.scoreValue = scoreValue;
    }

    public int getDirection() {
        return direction;
    }

    public int getGridX() {
        return gridX;
    }

    public void setGridX(int gridX) {
        this.gridX = gridX;
    }

    public int getGridY() {
        return gridY;
    }

    public void setGridY(int gridY) {
        this.gridY = gridY;
    }
    public float getyRandomFloat() {
        return yRandomFloat;
    }


    public float getxRandomFloat() {
        return xRandomFloat;
    }

    public int getCashValue() {
        return cashValue;
    }

    public void setCashValue(int cashValue) {
        this.cashValue = cashValue;
    }






}
