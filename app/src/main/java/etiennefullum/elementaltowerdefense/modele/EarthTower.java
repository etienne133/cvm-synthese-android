package etiennefullum.elementaltowerdefense.modele;

import etiennefullum.elementaltowerdefense.Activity.GameActivity;

/**
 * Created by etien on 13/12/16.
 */

public class EarthTower extends BasicTower {
    private double multiplierAgainsBoss;


    public EarthTower(int posX, int poxY) {
        super(posX, poxY);
        this.setPosX(posX);
        this.setPosY(poxY);
        this.setAtkValue(15);
        this.setRange(5);
        this.setAtkspeed(15);
        this.multiplierAgainsBoss = 2;

    }
    public void upgrade(){
        System.out.println("Upgrade de terre ");
        GameActivity.theGame.setCash(GameActivity.theGame.getCash()-this.getUpgradeCost());

        this.setValue(this.getValue()+this.getUpgradeCost());
        this.setAtkValue(this.getAtkValue()+this.getAtkValue()/100*5);
        this.multiplierAgainsBoss += 0.5;
        addSize();
        if(this.getAtkspeed()>0){
            this.setAtkspeed(this.getAtkspeed()-1);
        }

    }

    public double getMultiplierAgainsBoss() {
        return multiplierAgainsBoss;
    }

    public void setMultiplierAgainsBoss(double multiplierAgainsBoss) {
        this.multiplierAgainsBoss = multiplierAgainsBoss;
    }


}
