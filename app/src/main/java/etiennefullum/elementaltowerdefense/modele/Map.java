package etiennefullum.elementaltowerdefense.modele;

/**
 * Created by etien on 25/10/16.
 */

public class Map {
    private int gridWidth;
    private int gridHeight;
    private int path[][];

    public Map(int gridWidth, int gridHeight, int[][] path) {
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        this.path = path;
    }

    public int getGridWidth() {
        return gridWidth;
    }

    public void setGridWidth(int gridWidth) {
        this.gridWidth = gridWidth;
    }

    public int getGridHeight() {
        return gridHeight;
    }

    public void setGridHeight(int gridHeight) {
        this.gridHeight = gridHeight;
    }

    public int[][] getPath() {
        return path;
    }

    public void setPath(int[][] path) {
        this.path = path;
    }



    /*ou les enemy spawn*/
    public int getXStart(){
        for (int j = 0 ; j < this.gridHeight; j++){
            for (int i = 0 ; i < this.gridWidth; i ++){
                if(this.path[j][i] == 9) {
                    return j;
                }
            }
        }
        return -1;
    }
    public int getYStart(){
        for (int j = 0 ; j < this.gridHeight; j++){
            for (int i = 0 ; i < this.gridWidth; i ++){
                if(this.path[j][i] == 9) {
                    return i;
                }
            }
        }
        return -1;
    }

}
