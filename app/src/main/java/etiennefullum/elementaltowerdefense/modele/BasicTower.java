package etiennefullum.elementaltowerdefense.modele;

import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import etiennefullum.elementaltowerdefense.Activity.GameActivity;

/**
 * Created by etien on 25/10/16.
 */

public class BasicTower extends Entity {
    static int idIndex = 0;
    private int id;
    private int range;



    private double value;
    private int atkValue;
    private int atkspeed;
    private int cooldownIndex;
    private int targetIndex;

    private int size;
    private double grosseur;
    final static int threadSpeed = 100;
    static ScheduledExecutorService threadEnemyCheck = Executors.newSingleThreadScheduledExecutor();

    public BasicTower(int posX, int poxY) {
        super(posX, poxY);
        idIndex++;
        this.setPosX(posX);
        this.setPosY(poxY);
        this.range = 4;
        this.targetIndex = 0 ;
        this.atkspeed = 10;  // 10 == 1 fois par seconde car le thread tic à tous les 100 ms
        this.cooldownIndex = 0; // s'incrémente à chaque tic
        this.atkValue = 5; //
        this.value = 50;
        this.grosseur = 0 ;
        this.size = 0;

    }
    public int getAtkValue() {
        return atkValue;
    }

    public void setAtkValue(int atkValue) {
        this.atkValue = atkValue;
    }

    public double getUpgradeCost(){
        //upgrade une tour coute 25% de sa valeur
        return 150.0+ ((double)this.getValue())/100*25;
    }

    // fonction qui ugrade une tour ;
    public void upgrade(){
        //va être redéfinie dans les classes qui hérite de basic tower .
    }


    public static void findIfInRange() {
        threadEnemyCheck.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try{

                Vector<BasicEnemy> listEnemies = GameActivity.theGame.getListEnemy();
                ArrayList<BasicTower> listTower = GameActivity.theGame.getListTower();
                if (!listEnemies.isEmpty()) {


                    for (int j = 0; j < listTower.size(); j++) {

                        if(listTower.get(j).getTargetIndex()>= listEnemies.size()){

                            listTower.get(j).setTargetIndex(0);
                        }


                        if(GameActivity.theGame.getListEnemy().size() >= listTower.get(j).getTargetIndex() ){
                            if(listTower.get(j).cooldownIndex >   listTower.get(j).atkspeed){
                                if ( listTower.get(j) instanceof CashTower){
                                    //Ajoute de l'argent par rapport à l'argent que le joueur à déjà. 2% quand la cashtower n'est pas upgrader.
                                    GameActivity.theGame.setCash(GameActivity.theGame.getCash() + GameActivity.theGame.getCash()/100.0*((CashTower) listTower.get(j)).getPourcentageCash());
                                    ((CashTower) listTower.get(j)).setTotalRemise(((CashTower) listTower.get(j)).getTotalRemise()+GameActivity.theGame.getCash()/100.0*((CashTower) listTower.get(j)).getPourcentageCash());
                                    listTower.get(j).setCooldownIndex(0);
                                }else{

                            int x1 = listTower.get(j).getPosX();
                            int x2 = GameActivity.theGame.getListEnemy().get(listTower.get(j).getTargetIndex()).getGridX();
                            int y1 = listTower.get(j).getPosY();
                            int y2 = GameActivity.theGame.getListEnemy().get(listTower.get(j).getTargetIndex()).getGridY();


                            double dx = Math.pow((x2 - x1), 2); //deltax
                            double dy = Math.pow((y2 - y1), 2); //deltay
                            double distance = Math.sqrt(dx + dy);



                            if (distance >= listTower.get(j).range) {
                              //  System.out.println("Switch target");
                                /*regarde la prochaine tour. */
                                listTower.get(j).addTargetIndex(1);

                            }else if (distance < listTower.get(j).range){//si dans le range
                               // si peut attaquer

                                   if(listTower.get(j) instanceof WaterTower){
                                       new Projectile(0,0,listTower.get(j),listEnemies.get(listTower.get(j).getTargetIndex()), listTower.get(j).getTargetIndex(),"water",listTower.get(j).getAtkValue(),((WaterTower) listTower.get(j)).getSlowValue());
                                   }
                                   else if ( listTower.get(j) instanceof FireTower){
                                       new Projectile(0,0,listTower.get(j), listTower.get(j).getTargetIndex(),"fire",listTower.get(j).getAtkValue());
                                   }
                                   else if ( listTower.get(j) instanceof CashTower){
                                       //Ajoute de l'argent par rapport à l'argent que le joueur à déjà. 2% quand la cashtower n'est pas upgrader.

                                       GameActivity.theGame.ajouterEnleverCash(GameActivity.theGame.getCash()/100.0*((CashTower) listTower.get(j)).getPourcentageCash());
                                   }else if(listTower.get(j) instanceof EarthTower){
                                       new Projectile(0,0,listTower.get(j),listEnemies.get(listTower.get(j).getTargetIndex()), listTower.get(j).getTargetIndex(),"earth",listTower.get(j).getAtkValue(),((EarthTower) listTower.get(j)).getMultiplierAgainsBoss());
                                   }
                                   else if( listTower.get(j) instanceof BasicTower){
                                       new Projectile(0,0,listTower.get(j), listTower.get(j).getTargetIndex(),"basic",listTower.get(j).getAtkValue());
                                   }


                                   listTower.get(j).setCooldownIndex(0);
                                }
                               }
                            }

                            listTower.get(j).addColdownTime(1);
                        }
                       // }
                    }

                }
                }catch(Exception e){e.printStackTrace();}
            }
        }, 0, threadSpeed, TimeUnit.MILLISECONDS);
    }


    public void addTargetIndex(int v) {
        this.targetIndex += v;
    }
    public void addColdownTime(int v){
        this.cooldownIndex += v;
    }


    public int getTargetIndex() {
        return targetIndex;
    }

    public void setTargetIndex(int targetIndex) {
        this.targetIndex = targetIndex;
    }



    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }


    public int getAtkspeed() {
        return atkspeed;
    }

    public void setAtkspeed(int atkspeed) {
        this.atkspeed = atkspeed;
    }

    public int getCooldownIndex() {
        return cooldownIndex;
    }

    public void setCooldownIndex(int cooldownIndex) {
        this.cooldownIndex = cooldownIndex;
    }
    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
    public void addSize(){
        this.size+=2;
    }



}






