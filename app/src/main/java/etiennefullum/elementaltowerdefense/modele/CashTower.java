package etiennefullum.elementaltowerdefense.modele;

import etiennefullum.elementaltowerdefense.Activity.GameActivity;

/**
 * Created by etien on 12/12/16.
 */

public class CashTower extends BasicTower {




    private double pourcentageCash;


    private double totalRemise;

    public CashTower(int posX, int poxY) {
        super(posX, poxY);
        this.setRange(0);
        this.setAtkspeed(250); // 250 == 1 fois par 10 secondes  car le thread tic à tous les 100 ms
        this.setAtkValue(0); //
        this.setValue(300);
        this.pourcentageCash = 15.0;

    }
    public void upgrade(){
        GameActivity.theGame.setCash(GameActivity.theGame.getCash()-this.getUpgradeCost());
        this.setValue(this.getValue()+this.getUpgradeCost());
        this.pourcentageCash += 5;
        addSize();
    }

    public double getPourcentageCash() {
        return pourcentageCash;
    }

    public void setPourcentageCash(double pourcentageCash) {
        this.pourcentageCash = pourcentageCash;
    }

    public double getTotalRemise() {
        return totalRemise;
    }

    public void setTotalRemise(double totalRemise) {
        this.totalRemise = totalRemise;
    }

}
