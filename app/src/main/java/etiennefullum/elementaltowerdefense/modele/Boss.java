package etiennefullum.elementaltowerdefense.modele;

/**
 * Created by etien on 25/10/16.
 */

public class Boss extends BasicEnemy {
    private boolean isBoss;
    public Boss(int hp  , Boolean isAlive, int scoreValue, int posX, int posY, int Direction, int gridX, int gridY, boolean isboss) {
        super(hp, isAlive, scoreValue, posX, posY, Direction, gridX, gridY);
        this.isBoss = isBoss;
        this.setCashValue(50);
    }

    public boolean isBoss() {
        return isBoss;
    }

    public void setBoss(boolean boss) {
        isBoss = boss;
    }
}
