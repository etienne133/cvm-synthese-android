package etiennefullum.elementaltowerdefense.modele;

import etiennefullum.elementaltowerdefense.Activity.GameActivity;

/**
 * Created by etien on 12/12/16.
 */

public class FireTower extends BasicTower{

    public FireTower(int posX, int poxY) {
        super(posX, poxY);
        this.setPosX(posX);
        this.setPosY(poxY);
        this.setAtkValue(12);
        this.setRange(5);

    }
    // surdéfinition de la méthode de basictower

    public void upgrade(){
        System.out.println("Upgrade de feu ");
        GameActivity.theGame.setCash(GameActivity.theGame.getCash()-this.getUpgradeCost());
        this.setValue(this.getValue()+this.getUpgradeCost());
        this.setAtkValue(this.getAtkValue()+this.getAtkValue()/100*7);
        if(this.getRange()<10){
            this.setRange(this.getRange()+1);

        }
        addSize();
        if(this.getAtkspeed()>2){
            this.setAtkspeed(this.getAtkspeed()-1);
        }
    }


}
