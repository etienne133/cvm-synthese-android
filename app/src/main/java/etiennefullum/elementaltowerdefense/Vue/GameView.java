package etiennefullum.elementaltowerdefense.Vue;

import android.annotation.SuppressLint;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.HapticFeedbackConstants;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.Random;


import etiennefullum.elementaltowerdefense.Activity.GameActivity;
import etiennefullum.elementaltowerdefense.R;
import etiennefullum.elementaltowerdefense.modele.BasicEnemy;
import etiennefullum.elementaltowerdefense.modele.BasicTower;
import etiennefullum.elementaltowerdefense.modele.Boss;
import etiennefullum.elementaltowerdefense.modele.CashTower;
import etiennefullum.elementaltowerdefense.modele.EarthTower;
import etiennefullum.elementaltowerdefense.modele.FireTower;

import etiennefullum.elementaltowerdefense.modele.Projectile;
import etiennefullum.elementaltowerdefense.modele.Sprinter;
import etiennefullum.elementaltowerdefense.modele.WaterTower;

/**
 * Created by etien on 25/10/16.
 */

/**
 * 2 = droite
 * 3 = bas
 * 4 = gauche
 * 5 = haut
 * 9 = départ
 * 10 = fin
 **/
public class GameView extends SurfaceView {

    private Bitmap bmp;
    private SurfaceHolder holder;


    private Paint paintGrid = new Paint();
    private Paint paintMap = new Paint();
    private Paint basicEnemy = new Paint();
    private Paint paintPath = new Paint();
    private Paint paintMenuBgColor = new Paint();
    private Paint paintBoss = new Paint();
    private Paint caseSelected = new Paint();
    private Paint paintWaterTower = new Paint();
    private Paint paintGold1 = new Paint();
    private Paint paintGold2 = new Paint();
    private Paint white = new Paint();
    private Paint darkGrey = new Paint();
    private Paint black = new Paint();
    private Paint paleBlue = new Paint();
    private Paint paleBrown = new Paint();
    private Paint waterBlue = new Paint();
    private Paint paleRed = new Paint();
    private Paint range = new Paint();
    private Paint grassGreen = new Paint();

    ArrayList<Paint> fireColorEffect = new ArrayList<Paint>();
    int listIndex;
    private Paint paintFireTower = new Paint();
    private Paint paintFireTone = new Paint();
    private Paint paintFireTone2 = new Paint();
    private Paint paintFireTone3 = new Paint();


    /*Pour éviter de redesinner */
    private boolean bgDrawn = false;
    private boolean gridDrawn = false;
    private boolean pathDrawn = false;

    int movingspeed = 500;


    final int gridX = 15;
    final int gridY = 21;


    float x;
    float y;


    int spawnLocationX;
    int spawnLocationY;
    Random r = new Random();
    Random rfloat = new Random();


    public GameActivity master;


    /*Je passe 'activité au constructeur pour avoir accès au bouton qui ne sont pas dans le canvas */
    public GameView(GameActivity g) {

        super(g);

        master = g;
        holder = getHolder();
        paintMap.setColor(ContextCompat.getColor(getContext(), R.color.sandPath));
        paintMenuBgColor.setColor(ContextCompat.getColor(getContext(), R.color.blueMenu));
        paintBoss.setColor(ContextCompat.getColor(getContext(), R.color.bossBrown));
        caseSelected.setColor(ContextCompat.getColor(getContext(), R.color.caseSelected));
        paintWaterTower.setColor(ContextCompat.getColor(getContext(), R.color.iceCold));
        paintFireTower.setColor(ContextCompat.getColor(getContext(), R.color.redFire));
        paintFireTone.setColor(ContextCompat.getColor(getContext(), R.color.redFire2));
        paintFireTone2.setColor(ContextCompat.getColor(getContext(), R.color.redFire3));
        paintFireTone3.setColor(ContextCompat.getColor(getContext(), R.color.redFire4));
        paintGold1.setColor(ContextCompat.getColor(getContext(), R.color.gold1));
        paintGold2.setColor(ContextCompat.getColor(getContext(), R.color.gold2));
        paleBrown.setColor(ContextCompat.getColor(getContext(), R.color.paleBrown));
        darkGrey.setColor(ContextCompat.getColor(getContext(), R.color.darkGrey));
        black.setColor(ContextCompat.getColor(getContext(), R.color.black));
        paleBlue.setColor(ContextCompat.getColor(getContext(), R.color.paleBlue));
        waterBlue.setColor(ContextCompat.getColor(getContext(), R.color.waterBlue));
        paleRed.setColor(ContextCompat.getColor(getContext(), R.color.paleRed));
        range.setColor(ContextCompat.getColor(getContext(), R.color.rangeBlue));
        grassGreen.setColor(ContextCompat.getColor(getContext(), R.color.grassGreen));
        white.setColor(Color.WHITE);
        range.setAlpha(50);
        fireColorEffect.add(paintFireTower);
        fireColorEffect.add(paintFireTone);
        fireColorEffect.add(paintFireTone2);
        fireColorEffect.add(paintFireTone3);
        listIndex = fireColorEffect.size();


        holder.addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
            }

            @SuppressLint("WrongCall")
            @Override
            public void surfaceCreated(SurfaceHolder holder) {

                Canvas c = holder.lockCanvas(null);
                setWillNotDraw(false);
                onDraw(c);
                holder.unlockCanvasAndPost(c);


            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format,
                                       int width, int height) {
            }


        });

    }

    @Override
    protected void onDraw(Canvas canvas) {
        float widthPx = canvas.getWidth();
        float heightPx = canvas.getHeight();
        x = widthPx / master.theGame.getWidth();
        y = heightPx / master.theGame.getHeight();
        Projectile.gameW = (int) (widthPx / master.theGame.getWidth());
        Projectile.gameH = (int) (heightPx / master.theGame.getHeight());
        master.largeurX = widthPx / master.theGame.getWidth();
        master.largeurY = heightPx / master.theGame.getHeight();


        try {
            canvas.drawRect(master.getCasePosXSelected() * x, master.getCasePosYSelected() * y, ((master.getCasePosXSelected() * x) + x), ((master.getCasePosYSelected() * y) + y), caseSelected);
            if (!bgDrawn) {
                canvas.drawColor(ContextCompat.getColor(getContext(), R.color.grassGreen));
                bgDrawn = true;
            }
            if (!gridDrawn) {
                paintGrid.setColor(Color.RED);
                paintGrid.setStrokeWidth(10);
                paintGrid.setAlpha(70);
                for (int i = 0; i < master.theGame.getWidth(); i++) {
                    canvas.drawLine(i * x, 0, i * x, heightPx, paintGrid);
                }
                for (int i = 0; i < master.theGame.getHeight(); i++) {
                    canvas.drawLine(0, i * y, widthPx, i * y, paintGrid);
                }
                gridDrawn = true;
            }

            for (int j = 0; j < gridY; j++) {
                for (int i = 0; i < gridX; i++) {
                    if (i == 0 || i == gridX - 1 || j == 0 || j == gridY - 1) {
                        canvas.drawRect(i * x, j * y, ((i * x) + x), ((j * y) + y), grassGreen);
                    }
                    if (master.theMap.getPath()[j][i] < 6 && master.theMap.getPath()[j][i] >= 1) {

                        canvas.drawRect(i * x, j * y, ((i * x) + x), ((j * y) + y), paintMap);
                    } else if (master.theMap.getPath()[j][i] == 10) {
                        paintPath.setColor(Color.RED);
                        canvas.drawRect(i * x, j * y, ((i * x) + x), ((j * y) + y), paintPath);
                    } else if (master.theMap.getPath()[j][i] == 9) {
                        spawnLocationX = i;
                        spawnLocationY = j;
                        canvas.drawRect(i * x, j * y, ((i * x) + x), ((j * y) + y), paintMap);

                    }
                }
            }
            if (master.findTowerPositionInListByPosition() != -1) {
                canvas.drawCircle(master.getCasePosXSelected() * x, master.getCasePosYSelected() * y, master.getTower().get(master.findTowerPositionInListByPosition()).getRange() * x, range);
            }

            if (!master.theGame.getListEnemy().isEmpty()) {
                basicEnemy.setColor(Color.RED);
                for (BasicEnemy e : master.theGame.getListEnemy()) {

                    if (e instanceof Boss) {
                        canvas.drawCircle(e.getGridX() * x + x, e.getGridY() * y, 60, paintBoss);
                    } else if (e instanceof Sprinter) {
                        canvas.drawCircle(e.getGridX() * x + e.getxRandomFloat() * x / 2, e.getGridY() * y + y / 2 * e.getyRandomFloat(), 20, range);

                    } else if (e instanceof BasicEnemy) {

                        canvas.drawCircle(e.getGridX() * x + e.getxRandomFloat() * x / 2, e.getGridY() * y + y / 2 * e.getyRandomFloat(), 20, basicEnemy);
                    }
                }
            }


            //ajouter un si pour les refresh
            for (BasicTower t : master.getTower()) {
                if (t instanceof WaterTower) {
                    drawPolygon(canvas, t.getPosX() * x + x / 2, t.getPosY() * y + y / 2, ((x + y) / 5 + t.getSize()), 5, 0, paintWaterTower);
                    drawPolygon(canvas, t.getPosX() * x + x / 2, t.getPosY() * y + y / 2, ((x + y) / 7 + t.getSize()), 5, 0, paleBlue);
                } else if (t instanceof FireTower) {
                    drawPolygon(canvas, t.getPosX() * x + x / 2, t.getPosY() * y + y / 2, ((x + y) / 4 + t.getSize()), 3, 0, paintFireTower);
                    drawPolygon(canvas, t.getPosX() * x + x / 2, t.getPosY() * y + y / 2, ((x + y) / 6 + t.getSize()), 3, 0, paleRed);
                } else if (t instanceof CashTower) {
                    drawPolygon(canvas, t.getPosX() * x + x / 2, t.getPosY() * y + y / 2, ((x + y) / 4 + t.getSize()), 8, 0, paintGold1);
                    drawPolygon(canvas, t.getPosX() * x + x / 2, t.getPosY() * y + y / 2, ((x + y) / 6 + t.getSize()), 8, 0, paintGold2);
                    black.setTextSize(20 + t.getSize());
                    canvas.drawText(String.format("%.2f", ((CashTower) t).getTotalRemise()), t.getPosX() * x + x / 4, t.getPosY() * y + y / 2, black);
                } else if (t instanceof EarthTower) {
                    drawPolygon(canvas, t.getPosX() * x + x / 2, t.getPosY() * y + y / 2, ((x + y) / 4 + t.getSize()), 4, 0, paintBoss);
                    drawPolygon(canvas, t.getPosX() * x + x / 2, t.getPosY() * y + y / 2, ((x + y) / 6 + t.getSize()), 4, 0, paleBrown);
                } else if (t instanceof BasicTower) {
                    drawPolygon(canvas, t.getPosX() * x + x / 2, t.getPosY() * y + y / 2, ((x + y) / 5), 5, 0, paintMenuBgColor);
                }
            }

            //mes liste ne sont pas threadsafe. je les copies pour la lectur
            LinkedList<Projectile> temp = (LinkedList<Projectile>) Projectile.projectileList.clone();
            for (Projectile p : temp) {
                if (p.getParentType().equals("water")) {
                    //effet d'un triangle qui tourne sur 360 deg.
                    drawPolygon(canvas, p.getPosX(), p.getPosY(), ((x + y) / 5), 3, r.nextInt(360), paintWaterTower);
                } else if (p.getParentType().equals("fire")) {
                    for (int i = 0; i <= 4; i++) {
                        //plusieurs petit projectile de couleur différente qui donne un effet de feu.
                        canvas.drawCircle(p.getPosX() + (rfloat.nextFloat() * x), p.getPosY() + (rfloat.nextFloat() * y), 8, fireColorEffect.get(r.nextInt(listIndex)));
                    }
                } else if (p.getParentType().equals("earth")) {
                    canvas.drawCircle(p.getPosX(), p.getPosY(), 17, paleBrown);
                    canvas.drawCircle(p.getPosX(), p.getPosY(), 8, paintBoss);
                } else {
                    canvas.drawCircle(p.getPosX(), p.getPosY(), 12, paintBoss);
                }


            }


            if (master.gameOver) {
                canvas.drawColor(ContextCompat.getColor(getContext(), R.color.black));
                white.setTextSize(75);
                canvas.drawText("Vous êtes mort ! ", 50, 200, white);
                canvas.drawText("Votre score est de : " + Integer.toString(master.theGame.getScore()), 50, 400, white);
                white.setTextSize(45);
                canvas.drawText("Appuiller n'importe ou pour retourner au menu", 50, 600, white);
            }

        } catch (ConcurrentModificationException e) {
            /*Je lance cette exception car ici, on est dans la vue et on effectue seulement des lectures sur les listes
            *Cette exception est du au faite que plusieurs thread touche au mêmes liste.Étant donné que c'est des lecture seulement, on peut l'ignorer
            * https://docs.oracle.com/javase/7/docs/api/java/util/ConcurrentModificationException.html
            *
            * */
            System.out.println("Modification concurente ");
        }


    }

    //Ecouteur du canvas :  doit être fait à l'intérieur de la vue
    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        float xe = event.getX();
        float ye = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (master.gameOver) {
                    master.onBackPressed();
                } else if (true) {
                /*Boucle pour trouver si l'on touche à une case */
                    trouver:
                    // optimisation : sort de la loop si trouver
                    for (int i = 0; i < 21; i++) {
                        for (int j = 0; j < 15; j++) {
                            if (j * x > xe && j + 1 * x < xe) {
                                if (i * y > ye && i + 1 * y < ye) {
                                    if (master.theGame.getGrid()[i - 1][j - 1] == 0) {// si sur une case vide -> verte
                                        master.setLastCaseValue(master.theGame.getGrid()[i - 1][j - 1]);
                                        master.setCasePosYSelected(i - 1);
                                        master.setCasePosXSelected(j - 1);
                                        master.setButtonOnEmptyCase();
                                        //master.theGame.setGridXY(master.getCasePosXSelected(), master.getCasePosYSelected(), 18);
                                        this.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
                                    } else if (master.theGame.getGrid()[i - 1][j - 1] == 7) { // 7 == case occupé mais qui peut être sélectionnér
                                        master.setLastCaseValue(master.theGame.getGrid()[i - 1][j - 1]);
                                        master.setCasePosYSelected(i - 1);
                                        master.setCasePosXSelected(j - 1);
                                        master.setButtonOnBasicTower();// la classe na pas besoin de la position de la tour car la classe connaie à qui fait référence master connait sa position
                                        this.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
                                    }
                                    break trouver;
                                }
                            }
                        }
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:

        }
        return true;
    }


    /*En android, sans utiliser de librairie/api spécial, les méthodes pour dessiner un polygone ou une forme n'existe pas à l'inverse du java habituel.
    * j'ai trouver une solution sur internet, que j'ai modifié pour qu'elle soie bonne pour mon projet
    * référence http://stackoverflow.com/questions/2047573/how-to-draw-filled-polygon */
    private void drawPolygon(Canvas mCanvas, float x, float y, float radius, float sides, float startAngle, Paint paint) {

        if (sides < 3) {
            return;
        }

        float a = ((float) Math.PI * 2) / sides;
        mCanvas.save();
        mCanvas.translate(x, y);
        mCanvas.rotate(startAngle);
        Path path = new Path();
        path.moveTo(radius, 0);
        for (int i = 1; i < sides; i++) {
            path.lineTo(radius * (float) Math.cos(a * i), radius * (float) Math.sin(a * i));
        }
        path.close();
        mCanvas.drawPath(path, paint);
        mCanvas.restore();
    }


}