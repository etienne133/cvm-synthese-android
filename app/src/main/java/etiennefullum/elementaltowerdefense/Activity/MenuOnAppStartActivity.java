package etiennefullum.elementaltowerdefense.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import etiennefullum.elementaltowerdefense.R;

public class MenuOnAppStartActivity extends AppCompatActivity {


    /*
    * Menu principal. Veiller notez que tous les objets relatif à android se retouve dans les fichier XML des Activitys
    * Les fonctions pour changez d'activity s'apelle du fichier XML.
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_on_app_start);
    }

    public void goToFieldChoice(View v){
        Intent intent = new Intent(v.getContext(), FieldChoiceActivity.class);
        startActivity(intent);
    }
    public void highScore(View v){
        Intent intent = new Intent(v.getContext(), HighScoreActivity.class);
        startActivity(intent);
    }
    public void option(View v){

    }
    public void quitter(View view){
        System.out.println("quitter!");
        finish();
    }
}
