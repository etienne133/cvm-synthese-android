package etiennefullum.elementaltowerdefense.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import etiennefullum.elementaltowerdefense.R;

public class FieldChoiceActivity extends AppCompatActivity {


    private String choix;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_field_choice);
    }
    public void choixFacile(View v){
        choix = "Facile";
        goToGame();
    }
    public void choixMoyen(View v){choix = "moyen";
        goToGame();}
    public void choixDifficile(View v){
        choix = "difficile";
        goToGame();
    }


    public void goToGame() {
        /*permet d'aller à l'activity GameActivity en fonction le la difficulté*/
        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra("Difficulty", choix);
        startActivity(intent);
    }
}
