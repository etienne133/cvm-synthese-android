package etiennefullum.elementaltowerdefense.Activity;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import etiennefullum.elementaltowerdefense.DAO.Operation;
import etiennefullum.elementaltowerdefense.R;

public class HighScoreActivity extends AppCompatActivity {
    ListView laListe;
    Operation op;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_score);
        laListe = (ListView)findViewById(R.id.listView);
        op = new Operation(this);
        op.ouvrirBD();
        //je laisse des ajouts à la db ici pour que tu n'ais pas à installer l'app et faire plusieurs juste pour voir qu'elle fonctionne .

        laListe.setBackgroundColor(Color.WHITE);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String> (this, android.R.layout.simple_list_item_1, op.getHighScore());
        laListe.setAdapter(arrayAdapter);
        op.fermerBD();
    }
}
