package etiennefullum.elementaltowerdefense.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import etiennefullum.elementaltowerdefense.DAO.Operation;
import etiennefullum.elementaltowerdefense.R;
import etiennefullum.elementaltowerdefense.Vue.GameView;
import etiennefullum.elementaltowerdefense.modele.BasicEnemy;
import etiennefullum.elementaltowerdefense.modele.BasicTower;
import etiennefullum.elementaltowerdefense.modele.Boss;
import etiennefullum.elementaltowerdefense.modele.CashTower;
import etiennefullum.elementaltowerdefense.modele.EarthTower;
import etiennefullum.elementaltowerdefense.modele.FireTower;
import etiennefullum.elementaltowerdefense.modele.Game;
import etiennefullum.elementaltowerdefense.modele.Map;
import etiennefullum.elementaltowerdefense.modele.Projectile;
import etiennefullum.elementaltowerdefense.modele.Sprinter;
import etiennefullum.elementaltowerdefense.modele.WaterTower;

import static etiennefullum.elementaltowerdefense.modele.Game.easyMap;
import static etiennefullum.elementaltowerdefense.modele.Game.enemyMoving;
import static etiennefullum.elementaltowerdefense.modele.Game.hardMap;
import static etiennefullum.elementaltowerdefense.modele.Game.mediumMap;
import static etiennefullum.elementaltowerdefense.modele.Game.waveData;
import static etiennefullum.elementaltowerdefense.modele.Game.waveStarted;

/**************************************************************************************************
 * Classe importante -Controlleur
 * Cette classe est particulièrement importante car c'est elle qui instancie la majorité des objets
 * du modèle. Elle est est passé en paramètre à la surface de dessin (Vue.GameView) pour qu'elle
 * accès au objets instanciés.
 * ************************************************************************************************
 **/
public class GameActivity extends AppCompatActivity {

    public static int diedEnemy;
    public Vector<BasicEnemy> enemy;
    public ArrayList<BasicTower> getTower() {
        return tower;
    }
    private ArrayList<BasicTower> tower;
    public static Game theGame;
    public Map theMap;
    private FrameLayout container;
    TextView textView;
    private Ecouteur ec;
    Operation op ;
    Random rn = new Random();


    /*J'utilise les 3 mêmes bouton étant donnée l'espace restreint et je les surdéfinie lorsque leur action doivent changer
    * par exemple s'il l'On clique sur une tour, Lun des boutons devien améliorer et un autre vendre*/

    private Button boutonGauche;
    private Button boutonMilieu;
    private Button boutonDroite;
    private TextView cashView;
    private TextView scoreView;
    private TextView niveauView;
    private TextView healthView;


    /*---*/
    public static float largeurX;
    public static float largeurY;

    /*---*/
    private int casePosXSelected;
    private int casePosYSelected;

    int enemyCount;
    int bossCount;
    int sprinterCount;
    
    //vitesse de 2 thread 
    int movingspeed = 120;
    int spawingspeed = 500;
    
    public boolean gameOver = false;
    public boolean start = false;
    public boolean go = false;
    public boolean lvlStarted = false;



    /*les threads*/
        /*la main loop prend plus de thread car elle s'occupe de la methode postInvalidate() qui refresh le canvas en entier*/

    // int thread = Runtime.getRuntime().availableProcessors();
    ScheduledExecutorService startWave ;
    ScheduledExecutorService waveMoving ;
    ScheduledExecutorService mainLoop ;


    public int getLastCaseValue() {
        return lastCaseValue;
    }

    public void setLastCaseValue(int lastCaseValue) {
        this.lastCaseValue = lastCaseValue;
    }

    private int lastCaseValue;

    public int getCasePosXSelected() {
        return casePosXSelected;
    }

    public void setCasePosXSelected(int casePosXSelected) {
        this.casePosXSelected = casePosXSelected;
    }

    public int getCasePosYSelected() {
        return casePosYSelected;
    }

    public void setCasePosYSelected(int casePosYSelected) {
        this.casePosYSelected = casePosYSelected;
    }



    /*--*/
    GameView v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startWave = Executors.newSingleThreadScheduledExecutor();
        waveMoving = Executors.newSingleThreadScheduledExecutor();
        mainLoop = Executors.newScheduledThreadPool(2);
        Projectile.threadProjectile = Executors.newScheduledThreadPool(2);
        op= new Operation(this);
        enemy = new Vector<BasicEnemy>();
        tower = new ArrayList<BasicTower>();
        Intent intent = getIntent();
        String name = intent.getStringExtra("Difficulty");
        if(name.equals("Facile")){
            theGame = new Game(false, easyMap, 15, 21, enemy, tower, 0);
            theMap = new Map(15, 21, easyMap);
        }else if(name.equals("moyen")){
            theGame = new Game(false, mediumMap, 15, 21, enemy, tower, 0);
            theMap = new Map(15, 21, mediumMap);
        }else if(name.equals("difficile")){
            theGame = new Game(false, hardMap, 15, 21, enemy, tower, 0);
            theMap = new Map(15, 21, hardMap);
        }


        //instantiation du thread qui cast les projectile d'une turrer vers un enemy
        Projectile.castProjectile();
        //instantiation du thread qui cherche l'enemie le plus proche, le plus avancée , dans son range
        BasicTower.findIfInRange();
        //pas de titre à l'activity
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // lien le fichier XML au lauyout
        setContentView(R.layout.activity_game);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Liens entere certains élément du XML aux variables
        boutonGauche = (Button) this.findViewById(R.id.buttonGame1);
        boutonMilieu = (Button) this.findViewById(R.id.buttonGame2);
        boutonDroite = (Button) this.findViewById(R.id.buttonGame3);


        cashView = (TextView) this.findViewById(R.id.textViewTheCash);
        scoreView = (TextView) this.findViewById(R.id.textViewTheScore);
        niveauView = (TextView) this.findViewById(R.id.textViewIngametheLVL);
        healthView = (TextView) this.findViewById(R.id.textTheViewHealth);

        boutonGauche.setEnabled(false);
        boutonMilieu.setEnabled(false);
        boutonDroite.setEnabled(false);

        boutonGauche.setText(" ");
        boutonMilieu.setText("   ");
        boutonDroite.setText("   ");

        textView = (TextView) findViewById(R.id.textView7);

        container = (FrameLayout) findViewById(R.id.layout_id);
        //Ajout de la surface de jeux à l'ativity
        v = new GameView(GameActivity.this);
        container.addView(v);

        //listener
        ec = new Ecouteur();
        boutonGauche.setOnClickListener(ec);
        boutonMilieu.setOnClickListener(ec);
        boutonDroite.setOnClickListener(ec);


        initialiser();
    }


    public void enemyMovement() throws ConcurrentModificationException {
        lvlStarted = true;
        if (waveStarted && !enemyMoving) {// spawn des enemys a chaque seconde
            waveStarted = false;
            System.out.println("départ theard de déplacment enemy");
            waveMoving.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    try {
                      //  movingspeed = 500;
                        int count = 0;
                        //itérateur pour rendre mon programme plus threadsafe
                        //http://stackoverflow.com/questions/13602204/iterating-priority-queue-throws-concurrentmodificationexception
                        Iterator<BasicEnemy> enemis = theGame.getListEnemy().iterator();
                        while (enemis.hasNext()) {
                            BasicEnemy e = enemis.next();
                            if (e.getHp() < 0) {
                                diedEnemy++;
                                enemis.remove();
                                theGame.getListEnemy().removeAll(Collections.singleton(null));
                                if(e instanceof Sprinter){
                                    theGame.setCash(theGame.getCash()+20);
                                }else if(e instanceof Boss){
                                    theGame.setCash(theGame.getCash()+250);
                                }
                                else{
                                    theGame.setCash(theGame.getCash()+10);
                                }
                                refreshCash();
                            } else {
                                if (e.getMouvementCooldownIndex() >= e.getMovementCooldown() ) {
                                    e.setMouvementCooldownIndex(0);
                                    if (theMap.getPath()[e.getGridY()][e.getGridX()] == 9) {
                                        e.setDirection(e.getDirection());
                                        e.addGrid();
                                    } else if (theMap.getPath()[e.getGridY()][e.getGridX()] == 1) {
                                        e.addGrid();
                                    } else if (theMap.getPath()[e.getGridY()][e.getGridX()] == 2) {
                                        e.setDirection(2);
                                        e.addGrid();
                                    } else if (theMap.getPath()[e.getGridY()][e.getGridX()] == 3) {
                                        e.setDirection(3);
                                        e.addGrid();
                                    } else if (theMap.getPath()[e.getGridY()][e.getGridX()] == 4) {
                                        e.setDirection(4);
                                        e.addGrid();
                                    } else if (theMap.getPath()[e.getGridY()][e.getGridX()] == 5) {
                                        e.setDirection(5);
                                        e.addGrid();
                                    } else if (theMap.getPath()[e.getGridY()][e.getGridX()] == 0) {
                                        System.out.println("Enemy out of bound error ");
                                    } else if (theMap.getPath()[e.getGridY()][e.getGridX()] == 10) {
                                        if (e.getAlive()) {
                                            diedEnemy++;
                                            e.setAlive(false);
                                            theGame.getListEnemy().remove(e);
                                            theGame.getListEnemy().removeAll(Collections.singleton(null));
                                        }
                                        refreshLife();
                                        theGame.setVie(theGame.getVie() - 1);
                                    }
                                } else {
                                    e.setMouvementCooldownIndex(e.getMouvementCooldownIndex() + e.getSpeed());
                                }

                            }


                        }
                    } catch (Exception e) {
                        System.out.println("Modification concurrente");
                    }

                }
            }, 0, movingspeed, TimeUnit.MILLISECONDS);


        }

    }

    //Thread qui s'occupe de faire apparaitre les enemies 
    public void enemySpawnLoop() {
        waveStarted = true;
        startWave.scheduleAtFixedRate(new Runnable() {


            @Override
            public void run() {
                try {
                    movingspeed = 500;

                    int spawnX = theMap.getXStart();
                    int spawnY = theMap.getYStart();
                    if(theGame.getLevel()<=20){//On peu facilement changez des parametre des ennemis ici
                        if (enemyCount < waveData[theGame.getLevel()][0]) {
                            theGame.getListEnemy().add(new BasicEnemy(10, true, 1, 1, 1, 3, spawnY, spawnX));
                            enemyCount++;
                            theGame.addScore(5);
                            refreshScore();
                        } else if (bossCount < waveData[theGame.getLevel()][2]) {
                            theGame.getListEnemy().add(new Boss(200, true, 1, 1, 1, 3, spawnY, spawnX, true));
                            bossCount++;
                            theGame.addScore(25);
                            refreshScore();
                        } else if (sprinterCount < waveData[theGame.getLevel()][1]) {
                            theGame.getListEnemy().add(new Sprinter(20, true, 1, 1, 1, 3, spawnY, spawnX));
                            sprinterCount++;
                            theGame.addScore(10);
                            refreshScore();
                        }
                    }else{//mode sans fin si le user termine 20 niveau. nombre d'unité = au niveau etype random enemis super puissant
                            int a = rn.nextInt(4);
                            if(a==1){
                                theGame.addScore(25);
                                theGame.getListEnemy().add(new Boss(600, true, 1, 1, 1, 3, spawnY, spawnX, true));
                                refreshScore();

                            }
                            else if(a==2){
                                theGame.addScore(5);
                                theGame.getListEnemy().add(new BasicEnemy(20, true, 1, 1, 1, 3, spawnY, spawnX));
                                refreshScore();

                            }
                            else if(a==3){
                                theGame.addScore(10);
                                theGame.getListEnemy().add(new Sprinter(40, true, 1, 1, 1, 3, spawnY, spawnX));
                                refreshScore();

                            }

                        }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, 0, spawingspeed, TimeUnit.MILLISECONDS);

    }

    //Écouteur pour cette activity gention du menus en abs du jeu
    private class Ecouteur implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            double cash = theGame.getCash();
            if (v.getId() == R.id.buttonGame1) {
                if (boutonGauche.getText().equals("GO")) {
                    go = true;
                } else if (boutonGauche.getText().equals("Tour (100)")) {
                    System.out.println(casePosXSelected);
                    System.out.println(casePosYSelected);
                    tower.add(new BasicTower(casePosXSelected, casePosYSelected));
                    theGame.setGridXY(getCasePosXSelected(), getCasePosYSelected(), 7);
                    theGame.setCash(theGame.getCash() - 100);
                    cashView.setText(String.valueOf(theGame.getCash()));
                    setButtonOnBasicTower();


                } else if (boutonGauche.getText().equals("Eau(200)")) {
                    if(cash>200) {
                        tower.set(findTowerPositionInListByPosition(), new WaterTower(getCasePosXSelected(), getCasePosYSelected()));
                        theGame.setCash(theGame.getCash() - 200.0);
                        setButtonOnBasicTower();
                    }
                } else if (boutonGauche.getText().equals("Améliorer")) {


                    if(cash>tower.get(findTowerPositionInListByPosition()).getUpgradeCost()){
                        tower.get(findTowerPositionInListByPosition()).upgrade();
                        cashView.setText(String.format("%.2f",theGame.getCash()));
                        boutonMilieu.setText(String.format("%.2f", tower.get(findTowerPositionInListByPosition()).getUpgradeCost()) + "$");
                    }
                }

            } else if (v.getId() == R.id.buttonGame2) {
                if (boutonMilieu.getText().equals("Feu(100)")) {
                    if(cash>100) {
                        tower.set(findTowerPositionInListByPosition(), new FireTower(getCasePosXSelected(), getCasePosYSelected()));
                        theGame.setCash(theGame.getCash() - 100.0);
                        setButtonOnBasicTower();
                    }
                } else if (boutonMilieu.getText().equals("Banque(300)")) {
                    if(cash>300){
                        tower.add(new CashTower(casePosXSelected, casePosYSelected));
                        theGame.setGridXY(casePosXSelected,casePosYSelected,7);
                        theGame.setCash(theGame.getCash() - 300.0);
                        setButtonOnBasicTower();
                    }
                }

            } else if (v.getId() == R.id.buttonGame3) {

                if (boutonDroite.getText().equals("Terre(100)")) {
                    if(cash>100) {
                        tower.set(findTowerPositionInListByPosition(), new EarthTower(getCasePosXSelected(), getCasePosYSelected()));
                        theGame.setCash(theGame.getCash() - 100.0);
                        setButtonOnBasicTower();
                    }
                } else if (boutonDroite.getText().equals("Vendre")) {
                    theGame.setCash(theGame.getCash()+tower.get(findTowerPositionInListByPosition()).getValue()/2);
                    theGame.setGridXY(getCasePosXSelected(),getCasePosYSelected(),0);
                    tower.remove(findTowerPositionInListByPosition());
                    tower.removeAll(Collections.singleton(null));
                    refreshCash();

                }
            }
        }
    }


    //Méthode qui initialise une partie
    public void initialiser() {
        niveauView.setText(String.valueOf(theGame.getLevel()));
        scoreView.setText(String.valueOf(theGame.getScore()));
        cashView.setText(String.valueOf(theGame.getCash()));
        healthView.setText(String.valueOf(theGame.getVie()));
        gameStart();
    }

    /*Main loop du jeu. */
    public void gameStart() throws ConcurrentModificationException {

        mainLoop.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                        if (!start) {
                            System.out.println("initialisation");
                            start = true;
                            initialiser();
                            theGame.setGameStarted(true);
                        }
                        if (!waveStarted) {// spawn des enemys a chaque seconde
                            System.out.println("enemieSPawn");
                            enemySpawnLoop();
                        }
                        if (!lvlStarted) {
                            System.out.println("lvlLoop");
                            enemyMovement();
                        }
                        if (waveData[theGame.getLevel()][0] + waveData[theGame.getLevel()][1] + waveData[theGame.getLevel()][2] == diedEnemy) {
                            //vérification si toute les enemis d'une wave sont mort.
                            theGame.addScore(50);
                            nextLevel();
                        }
                        if (theGame.getVie() <= 0) {
                            //condition de défaite
                            if (!gameOver) {
                                gameOver = true;
                                startWave.shutdownNow();
                                waveMoving.shutdownNow();
                                mainLoop.shutdownNow();
                                op.ouvrirBD();
                                op.addHighScore(theGame.getScore());
                                op.fermerBD();

                            }
                        }
                /*refresh du canvas,*/
                        v.postInvalidate();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }, 0, 50, TimeUnit.MILLISECONDS);
    }

    /*Prochaine wave d'enemie*/
    public void nextLevel() {
        theGame.setLevel(theGame.getLevel() + 1);

        theGame.setCash(theGame.getCash()+100);
        refreshCash();
        refreshUI();
        enemy.clear();
        diedEnemy = 0;
        enemyCount = 0;
        bossCount = 0;
        sprinterCount = 0;


    }


    public void setButtonOnEmptyCase() {
        boutonMilieu.setEnabled(true);
        boutonGauche.setEnabled(true);
        boutonGauche.setText("Tour (100)");
        boutonMilieu.setText("Banque(300)");
        boutonDroite.setText("   ");
        boutonDroite.setEnabled(false);

    }

    public void setButtonOnBasicTower() {
        if (tower.get(findTowerPositionInListByPosition()).getClass().equals(BasicTower.class)) {
            boutonGauche.setText("Eau(200)");
            boutonMilieu.setText("Feu(100)");
            boutonDroite.setText("Terre(100)");
            boutonGauche.setEnabled(true);
            boutonMilieu.setEnabled(true);
            boutonDroite.setEnabled(true);

        } else {
            boutonGauche.setText("Améliorer");
            boutonMilieu.setText(String.format("%.2f", tower.get(findTowerPositionInListByPosition()).getUpgradeCost()) + "$");
            boutonDroite.setText("Vendre");
            boutonMilieu.setEnabled(false);
            boutonDroite.setEnabled(true);
            boutonGauche.setEnabled(true);

        }
        cashView.setText(String.format("%.2f",theGame.getCash()));
    }


    public void setButtonOnTower() {
        boutonGauche.setText("Améliorer");
        boutonMilieu.setText(String.valueOf(tower.get(findTowerPositionInListByPosition()).getUpgradeCost()) + "$");
        boutonDroite.setText("Vendre");
        boutonMilieu.setEnabled(false);
        boutonDroite.setEnabled(true);
        boutonGauche.setEnabled(true);

    }


    public int findTowerPositionInListByPosition() {
        int x = getCasePosXSelected();
        int y = getCasePosYSelected();
        for (int i = 0; i < tower.size(); i++) {
            if (x == tower.get(i).getPosX() && y == tower.get(i).getPosY()) {
                return i;
            }
        }
        return -1;
    }

    // Obligatoire de faire un runnable sur runOnUiThread pour modifier des boutons l'interface si l'on n'est pas sur l'interface (dans ce cas-ci la surface de jeu)
    //(because android)
    //ref : http://stackoverflow.com/questions/5161951/android-only-the-original-thread-that-created-a-view-hierarchy-can-touch-its-vi
    private void refreshUI() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                niveauView.setText(String.valueOf(theGame.getLevel()));
                scoreView.setText(String.valueOf(theGame.getScore()));

            }
        });
    }

    public void refreshCash() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cashView.setText(String.format("%.2f",theGame.getCash()));
            }
        });
    }
    public void refreshLife() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                healthView.setText(Integer.toString(theGame.getVie()));
            }
        });
    }

    public void refreshScore() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                scoreView.setText(String.valueOf(theGame.getScore()));

            }
        });
    }
    //surdéfinition  de onbackpressed
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        System.exit(0);
    }




}
