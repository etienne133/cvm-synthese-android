package etiennefullum.elementaltowerdefense.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by etien on 14/12/16.
 */

public class Operation {
    private DatabaseHelper helper;
    private SQLiteDatabase database;

    public Operation(Context contexte) {
        helper = new DatabaseHelper(contexte);
    }

    public void ouvrirBD(){

        database = helper.getWritableDatabase();
    }
    public void fermerBD(){

        database.close();
    }
    public ArrayList<String> getHighScore(){
        ArrayList<String> a = new ArrayList<String>();
        Vector<Hashtable<String, Object>> v = new Vector<Hashtable<String, Object>>();
        Cursor c = database.rawQuery("SELECT * FROM HIGHSCORE",null);

        while(c.moveToNext()) {
            a.add(Integer.toString(c.getInt(1))+" le " + c.getString(2));
        }
        return a;
    }
    public void addHighScore (int s){
        ContentValues cv = new ContentValues();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String date = df.format(c.getTime());
        System.out.println(date);
        cv.put("score",s);
        cv.put("date",date);
        database.insertOrThrow("HIGHSCORE", null, cv);
    }


}
