package etiennefullum.elementaltowerdefense.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by etien on 14/12/16.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public DatabaseHelper(Context context) {
        super(context, "db", null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE HIGHSCORE (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "score INTEGER," +
                        "date TEXT);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        /*Deleter la table*/
        System.out.println("Table vidée! ");
        db.execSQL("DROP TABLE IF EXISTS HIGHSCORE");
        onCreate(db);
    }


}


